//
//  InstagramHandler.swift
//  TheNews
//
//  Created by Arnita Martiana on 13/12/21.
//  Copyright © 2021 Arnita Martiana. All rights reserved.
//

import UIKit

class InstagramHandler: NewsTableHandler {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InstagramCell.identifier) as! InstagramCell

        let article = articles[indexPath.row]
        cell.load(article: article)
        cell.loadLogo(urlString: article.urlToSourceLogo,
                      size: InstagramCell.logoSize)

        return cell
    }

}
